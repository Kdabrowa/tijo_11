package pl.edu.pwsztar.chess;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;

@AllArgsConstructor
@Getter
public class Convert {
    public int x;
    public int y;

    public int x_out;
    public int y_out;

    public Convert(FigureMoveDto figureMoveDto){
        String temp = figureMoveDto.getSource();
        String temp2 = figureMoveDto.getDestination();
        this.x= (int)temp.charAt(0)-96;
        this.y = Integer.parseInt(temp.substring(2));
        this.x_out = (int)temp2.charAt(0)-96;
        this.y_out = Integer.parseInt(temp2.substring(2));
    }

    @Override
    public String toString() {
        return "Convert{" +
                "x=" + x +
                ", y=" + y +
                ", x_out=" + x_out +
                ", y_out=" + y_out +
                '}';
    }
}