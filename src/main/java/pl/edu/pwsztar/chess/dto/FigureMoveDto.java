package pl.edu.pwsztar.chess.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@ToString
public class FigureMoveDto implements Serializable {
    private String source;
    private String destination;
    private FigureType type;

    public void setSource(String source){
        this.source = source;
    }

    public String getSource(){
        return source;
    }

    public void setDestination(String destination){
        this.destination = destination;
    }

    public String getDestination(){
        return destination;
    }

    public void setType(FigureType type){
        this.type = type;
    }

    public FigureType getType() {
        return type;
    }
}
