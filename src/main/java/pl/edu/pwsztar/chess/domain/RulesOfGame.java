package pl.edu.pwsztar.chess.domain;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * source na destination w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(Point source, Point destination);

    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() != destination.getX()  && source.getY() != destination.getY()) {
                System.out.println("zły ruch");
                return false;
            }
            return Math.abs(destination.getX() - source.getX()) !=
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() != destination.getX()  && source.getY() != destination.getY()){
                System.out.println("zły ruch");
                return false;
            }
            else if(source.getX() == destination.getX() && source.getY() == destination.getY()){
                System.out.println("zły ruch");
                return false;
            }
            return true;
        }
    }

    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                return false;
            }

            return Math.abs(destination.getX() - source.getX()) ==
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() != destination.getX()  || (destination.getY() - source.getY())!=1) {
                System.out.println("zły ruch");
                return false;
            }
            return Math.abs(destination.getX() - source.getX()) !=
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(Math.abs(destination.getX() - source.getX())>1  || (Math.abs(destination.getY() - source.getY()))>1) {
                System.out.println("zły ruch");
                return false;
            }
            return true;
        }
    }

    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            // TODO: Prosze dokonczyc implementacje
            return true;
        }
    }


    // TODO: Prosze dokonczyc implementacje kolejnych figur szachowych: Knight, King, Queen, Rook, Pawn
    // TODO: Prosze stosowac zaproponowane nazwy klas !!!
    // TODO: Kazda klasa powinna implementowac interfejs RulesOfGame
}
