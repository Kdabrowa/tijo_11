package pl.edu.pwsztar.chess.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.chess.Convert;
import pl.edu.pwsztar.chess.domain.Point;
import pl.edu.pwsztar.chess.domain.RulesOfGame;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;
import pl.edu.pwsztar.chess.service.ChessService;

import static pl.edu.pwsztar.chess.dto.FigureType.BISHOP;
import static pl.edu.pwsztar.chess.dto.FigureType.KNIGHT;

@Service
@Transactional
public class ChessServiceImpl implements ChessService {
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame rook;
    private RulesOfGame pawn;
    private RulesOfGame king;
    private RulesOfGame queen;
    // ...

    public ChessServiceImpl() {
        bishop = new RulesOfGame.Bishop();
        knight = new RulesOfGame.Knight();
        rook = new RulesOfGame.Rook();
        pawn = new RulesOfGame.Pawn();
        king = new RulesOfGame.King();
        queen = new RulesOfGame.Queen();
        // ...
    }

    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        Convert convert = new Convert(figureMoveDto);
        // refaktoryzacja?
        switch (figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(new Point(convert.x, convert.y), new Point(convert.x_out, convert.y_out));
            case KNIGHT:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return knight.isCorrectMove(new Point(convert.x, convert.y), new Point(convert.x_out, convert.y_out));
            case ROOK:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return rook.isCorrectMove(new Point(convert.x, convert.y), new Point(convert.x_out, convert.y_out));
            case PAWN:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return pawn.isCorrectMove(new Point(convert.x, convert.y), new Point(convert.x_out, convert.y_out));
            case KING:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return king.isCorrectMove(new Point(convert.x, convert.y), new Point(convert.x_out, convert.y_out));
            case QUEEN:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return queen.isCorrectMove(new Point(convert.x, convert.y), new Point(convert.x_out, convert.y_out));

        }

        return false;
    }
}
